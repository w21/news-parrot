# See the file COPYRIGHT for the license terms of this software.

########### START CONFIGURATION SECTION
# These are configurable variables. The defaults may not suit you,
# so change them.

# these are the groups from which postings are answered; separate
# multiple group names with commas, but no blanks (like foo.test,bar.test)
TESTGROUPS = example.test

# the name of your news server
NEWSSERVER = news.example.org

# where INN keeps its binaries; ignore this if you don't use INN or
# sbin/parrot-wrapper
NEWSBINDIR = /usr/lib/news/bin

# the From address of the auto-responses
RESPONDER_ADDR = parrot@news.example.org

# installation directories:
BASEDIR = /opt/example
# for the programs
SBINDIR = $(BASEDIR)/sbin
# for the get_opts module
LIBDIR  = $(BASEDIR)/lib/python3
# for the config file and example template (the latter being unused
# in the default configuration)
ETCDIR  = $(BASEDIR)/etc
#
############ END CONFIGURATION SECTION

VERSION = `git describe --dirty`
PPROGRAMS = sbin/parrot
SPROGRAMS = $(PPROGRAMS) sbin/parrot-wrapper
STAGING = staging-dir

all: check

push:
	git push --all
	git push --tags
	git push --all gitlab
	git push --tags gitlab

check:
	for i in $(PPROGRAMS); do python3 -m py_compile $$i; done

install: check staging
	install -c $(STAGING)/sbin/* $(SBINDIR)
	install -c $(STAGING)/etc/* $(ETCDIR)
	install -c $(STAGING)/lib/python3/* $(LIBDIR)

staging:
	# do initial configuration and staging of the configured file
	rm -rf $(STAGING)
	mkdir -p $(STAGING)/sbin $(STAGING)/lib/python3 $(STAGING)/etc
	cp ../site-py/get_opts.py $(STAGING)/lib/python3
	sed -e "s|/opt/w21/etc|$(ETCDIR)|"          \
	    -e "s|/opt/w21/lib/python3|$(LIBDIR)|"  \
	    -e "s|@VERSION@|$(VERSION)|"            \
	    sbin/parrot > $(STAGING)/sbin/parrot
	sed -e "s|/opt/w21/sbin|$(SBINDIR)|"        \
	    -e "s|@VERSION@|$(VERSION)|"            \
	    -e "s|/usr/lib/news/bin|$(NEWSBINDIR)|" \
	    sbin/parrot-wrapper > $(STAGING)/sbin/parrot-wrapper
	sed -e "s|/opt/w21/etc|$(ETCDIR)|"          \
	    -e "s|@VERSION@|$(VERSION)|"            \
	    -e "s|parrot@news.example.org|$(RESPONDER_ADDR)|" \
	    -e "s|news.example.org|$(NEWSSERVER)|"  \
	    etc/parrot.conf > $(STAGING)/etc/parrot.conf.example
	sed -e "s|@VERSION@|$(VERSION)|"            \
	    etc/parrot-template > $(STAGING)/etc/parrot-template.example
	sed -e "s|/opt/w21/etc|$(ETCDIR)|"          \
	    -e "s|@VERSION@|$(VERSION)|"            \
	    -e "s|/opt/w21/sbin|$(SBINDIR)|"        \
	    -e "s|example.test|$(TESTGROUPS)|"      \
	    etc/parrot-program-feed > $(STAGING)/etc/parrot-program-feed.example
	chmod +x $(STAGING)/sbin/*

clean:
	find . -type f -name '*~' -exec rm {} +
	rm -rf $(STAGING)
