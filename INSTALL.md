-*- text -*-

See the file COPYRIGHT for the license terms of this software.

Installation Instructions
=========================

To install parrot on your news server, you need the privileges to
make changes to the news server configuration. (This does not
necessarily mean root, depending on your news installation.)

Dependencies
------------

  * Python 3.4 or later. If you don't have it, run `apt-get install
    python3` or whatever is appropriate for your OS; this should
    give you a new enough Python version with a semi-current OS
    installation. (Debian Wheezy is too old, though.)

    If you don't have root privileges, install an appropriate Python
    version in a private directory and make sure it is in the `PATH`
    as `python3` when `parrot` is called. The wrapper script
    `parrot-wrapper` is a good place to do that.

  * The `get_opts` module from my `site-py` repository. Clone its
    repository into the same parent directory as `news-parrot` and
    keep the name `site-py` so the parrot installation can find it:

    `(cd ..; git clone https://gitlab.com/w21/site-py.git)`

Configuration
-------------

Look into the Makefile and change the variables `TESTGROUPS`,
`NEWSSERVER`, `NEWSBINDIR`, `RESPONDER`, `BASEDIR`, `SBINDIR`,
`LIBDIR`, and `ETCDIR` as necessary to reflect your installation.
Make sure these directories exist. (The install procedure won't
create them automatically as a safeguard against typos.)

Installation
------------

`make install` will install the program and supporting files in the
directories you have configured in the Makefile.

The configuration files `parrot.conf` and `parrot-template` will be
installed as `parrot.conf.example` and `parrot-template.example` in
the ETCDIR, such that no previous configuration is overwritten. You
will need to rename them to their proper names (and then perhaps
edit them) yourself. 

Configuration of the News Server
--------------------------------

If you don't run INN as your news server, you'll have to figure out
for yourself how to call `parrot` for every posting.

With INN version 2, you can call `parrot` via a program feed. An
example `newsfeeds` snippet is provided in
`etc/parrot-program-feed`. The called program, `parrot-wrapper`, is
called with a storage API token of the posting as its single
argument. It then uses INN's storage manager `sm` to feed the
posting to `parrot`.

The change to the news server configuration is not automated.

Debugging
---------

In case you need to do any debugging, `parrot`'s command-line options
may be useful:

    usage: parrot [-lvvM] [-r rcpt_override]
      -l: log sent messages to syslog
      -r: recipient override (for testing)
      -v: be chatty; -vv: debug
      -M: don't send mail, print message instead

[ni@w21.org 2017-08-27]
