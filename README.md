-*- text -*-

The News Parrot
===============

The News Parrot (short `parrot`) is a Usenet test posting
auto-responder. It is intended to be installed on a news server such
that it is called for each posting to specific groups (like
`example.test`, for instance), which is then fed to its standard
input.

Parrot then sends a reply message to the poster via email, detailing
the path the posting took to arrive at the server and quoting the
original posting. This can be customized using a message template.

A reply message is not sent if one of the strings "ignore", "no
reply", or "no replies" occurs anywhere in the posting, or if the
posting contains a "Control" header, or an "Auto-Submitted" header
with a value other than "no". This is not intended to be customised.

Installation
------------

See the `INSTALL` file for installation instructions. There should
be no dependencies besides Python 3 and the `get_opts` module from
<https://gitlab.com/w21/site-py>. Ah, and a news server.

Bugs etc.
---------

The program isn't rigorously tested (this is a CFT project, after
all), but at the moment, I am not aware of any bugs. If you notice
any, I'd appreciate a report via email (see below) or an issue at
<https://gitlab.com/w21/news-parrot/issues>. Also, comments.

License
-------

See the file `COPYRIGHT` for the license terms of this software.
TL;DR: 2-clause BSD.

Author
------

Juergen Nickelsen <ni@w21.org>
